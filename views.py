from flask import render_template, flash, redirect
from app import app

@app.route('/', methods=('GET',) )
def index():
    return render_template("index.html",
                           title = "Home",
                          )

@app.route('/truc', methods=('GET',) )
def perdu():
    return "Tu t'es perdu", 404
